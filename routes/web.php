<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//montir
$router->post('/montir','MontirController@create');
$router->get('/montir','MontirController@read');
$router->post('/montir/{id}','MontirController@update');
$router->delete('/montir/{id}','MontirController@delete');
$router->get('montir/{id}','MontirController@detail');

//Spare
$router->post('/spare','SpareController@create');
$router->get('/spare','SpareController@read');
$router->post('/spare/{id}','SpareController@update');
$router->delete('/spare/{id}','SpareController@delete');
$router->get('spare/{id}','SpareController@detail');