<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

Class Montir extends Model
{

  public $table = 't_montir';

  protected $fillable = ['nama_montir','umur','alamat','pendidikan'];

}
