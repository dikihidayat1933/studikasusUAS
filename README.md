# Tugas Terakhir Pwd

Aplikasi ini sebuah aplikasi simpel berbasis web yang dibuat menggunakan lumen untuk backend dan 
vue.js untuk frontend. Aplikasi digunakan untuk menyimpan data penting yang menyangkut  bengkel.
Seperti : 
1. Sparepart
          
2. Montir
          


Your lumen project must use localhost;

Steps : 


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

# UI APLIKASI

## 1. Beranda
Beranda ini digunakan sebagai identitas web dan hal pertama untuk menarik seseorang dalam sebuah web.
![Beranda](ssaplikasi/1.PNG)

## 2. Read Data (Fitur Sparepart)
Sebuah page yang berisi data data sparepart yang didalamnya dapat melakukan sebuah crud.
![ReadData](ssaplikasi/2.PNG)

## 3. Create Data (Fitur Sparepart)
Sebuah page form untuk melakukan sebuah penambahan data sparepart dan lansung bertambah kedatabase.
![ReadData](ssaplikasi/3.PNG)

## 4. Edit Data (Fitur Sparepart)
Sebuah page form untuk melakukan sebuah Perubahan han data sparepart dan lansung berubah didatabasenya pun .
![ReadData](ssaplikasi/4.PNG)

Dan ini sedikit penjelasan tentang aplikasi bengkel ini semoga membantu dan berguna bagi anda. 