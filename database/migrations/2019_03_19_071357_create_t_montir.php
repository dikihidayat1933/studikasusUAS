<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTMontir extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_montir', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_montir',150);
            $table->string('umur',50);
            $table->text('alamat');
            $table->string('pendidikan',150);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_montir');
    }
}
