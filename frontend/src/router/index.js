import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Montir from '@/components/Montir'
import MontirForm from '@/components/MontirForm'
import Spare from '@/components/Spare'
import SpareForm from '@/components/SpareForm'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/montir',
      name: 'Montir',
      component: Montir
    },
    {
      path: '/montir/create',
      name: 'MontirCreate',
      component: MontirForm
    },
    {
      path: '/montir/:id',
      name: 'MontirEdit',
      component: MontirForm
    },
    {
      path: '/spare',
      name: 'Spare',
      component: Spare
    },
    {
      path: '/spare/create',
      name: 'SpareCreate',
      component: SpareForm
    },
    {
      path: '/spare/:id',
      name: 'SpareEdit',
      component: SpareForm
    }
  ]
})
